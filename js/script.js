;(function($) {

    var menu = [
        $(".products__logo div:nth-child(1)").html(),
        $(".products__logo div:nth-child(2)").html(),
        $(".products__logo div:nth-child(3)").html()
    ]
    var classes = [
        $(".products__logo div:nth-child(1)").attr('class'),
        $(".products__logo div:nth-child(2)").attr('class'),
        $(".products__logo div:nth-child(3)").attr('class')
    ]

    var swiper = new Swiper ('.swiper-container', {
        effect: 'fade',
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: function (index, className) {
                return (
                    `<div class="${classes[index] + " " + className}">
                        ${menu[index]}
                    </div>`
                )
            },
        },
    });

    if($(window).scrollTop() > 50){
        $(".page__header").addClass("fixed--header");
    }
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll > 50) {
            $(".page__header").addClass("fixed--header");
        }else{
            $(".page__header").removeClass("fixed--header");
        }
    })


    var teamSlider = new Swiper('.team__swiper-container', {
        infinite: true,
        effect: 'fade',
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.team__swiper-pagination',
            // type: 'fraction',
        },
        navigation: {
            nextEl: '.team__swiper-button-next',
            prevEl: '.team__swiper-button-prev',
        },
    });


}(jQuery));